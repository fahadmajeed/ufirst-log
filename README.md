# Log importer and analytics

This is a demo of log file import into a JSON file and a 
front-end for analysis of requests based on logs.

## Business logic and hierarchical setup
Currently, we are only catering one level of organizational hierarchy/nesting.

## Requirements

For development, you will only need Node.js, NPM, installed in your environement.


## Install

    $ git clone https://github.com/fahadmajeed/log-analyzer
    $ cd log-analyzer
    $ npm install

## Run import
After installation is done, on command line / terminal
run 
$ node src/start.js

this should import all the data from epa-http.txt into access_log_EPA_Jul95_parsed.json

## See the log dashboard

To see the dashboard or graphs, pie chart and line graph etc, 
simply open epa.html into chrome browser as it currently is targetted for Chrome