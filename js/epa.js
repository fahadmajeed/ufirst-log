jQuery(document).ready(function() {
  var epaDataRecords = JSON.parse(window.epadata);

  //show HTTP method types
  var requestCountObj = extractMethodCounts();
  let methodVals = [];
  let methodLabels = [];
  for (let [key, value] of Object.entries(requestCountObj)) {
    const label = (key=='INVALID-REQUEST') ? `${key}-${value}`: key;
    methodLabels.push(label);
    methodVals.push(value);
  }
  var ctx = document.getElementById('methodChart').getContext('2d');
  var data = {
    datasets: [{
        data: methodVals,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'red'
        ]
    }],
    labels: methodLabels
  };
  new Chart(ctx, {
    type: 'pie',
    data: data
  });

  // show requests per minute

  var reqsPerMinute = extractRequestsPerMinute();
  var requestsPerMinuteData = [];
  var minuteRequestLables = [];
  var timeFormat = 'MM/DD/YYYY HH:mm';

  for (let [key, value] of Object.entries(reqsPerMinute)) {
    const [day, hour, minute]= key.split(':');
    const dateObj = getDate(day, hour, minute);
    requestsPerMinuteData.push({x: dateObj, y: value});
    minuteRequestLables.push(newDateString(day, hour, minute));
  }

  var config = {
    type: 'line',
    data: {
      labels: minuteRequestLables,
      datasets: [{
        label: 'requests per minute',
        backgroundColor: 'yellow',
        borderColor: 'red',
        fill: false,
        data: requestsPerMinuteData,
      }]
    },
    options: {
      title: {
        text: 'Time Scale'
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            displayFormats: {
                quarter: 'hA'
            }
        },
          scaleLabel: {
            display: true,
            labelString: 'Minute'
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'value'
          }
        }]
      },
    }
  };
  
  var ctx2 = document.getElementById('requestsPerMinuteChart').getContext('2d');

  new Chart(ctx2, config);

  // show http code distributions
  var responseCount = extractResponseCodes();
  let responseVals = [];
  let responseLabels = [];
  for (let [key, value] of Object.entries(responseCount)) {
    responseLabels.push(key);
    responseVals.push(value);
  }

  var ctx3 = document.getElementById('responseCodeChart').getContext('2d');
  methodVals[3] = 30;
  data = {
    datasets: [{
        data: responseVals,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'red'
        ]
    }],
    labels: responseLabels
  };
  new Chart.PolarArea(ctx3, {
    data: data
  });

  // show bar chart for response size etc
  var filteredSizes = epaDataRecords.filter(record => (record.response_code == '200' && record.document_size < 1000))
  .reduce(function (acc, curr) {
      var key = `${curr.document_size}`;
      if (!acc[key]) {
        acc[key] = 1;
      } else {
        acc[key]++;
      }
   
     return acc;
   }, {});
   let docVals = [];
   let docLabels = [];
   var barData = [];
   for (let [key, value] of Object.entries(filteredSizes)) {
     docLabels.push(parseInt(key));
     docVals.push(parseInt(value));
     barData.push({x: key.toString(), y: value.toString()})
   }

  // requests per minute stacked bar chart
  var ctx4 = document.getElementById('documentSizeChart').getContext('2d');

  new Chart(ctx4, {
    type: 'line',
    data: {
      labels: docLabels,
      datasets: [{
        label: 'Doc size distributions',
        backgroundColor: 'red',
        borderColor: 'yellow',
        data: docVals,
        fill: false,
        showLine: true 
      }]
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Doc Size Distributions'
      },
      scales: {
        xAxes: [{
          display: true,
        }],
        yAxes: [{
          display: true,
          type: 'logarithmic',
        }]
      }
    }
  });

  // some Utility functions 
  function getDate(day, hour, minute) {
    return moment([1995, 07, day, hour, minute]).toDate();
  }

  function newDateString(day, hour, minute) {
    return moment([1995, 07, day, hour, minute]).format(timeFormat);
  }

  function extractMethodCounts() {
   return epaDataRecords.reduce(function (acc, curr) {
     if (curr.request.method) {
        if (!acc[curr.request.method]) {
          acc[curr.request.method] = 1;
        } else {
          acc[curr.request.method]++;
        }
     }
      return acc;
    }, {});
  }

  function extractResponseCodes() {
    return epaDataRecords.reduce(function (acc, curr) {
      if (['400', '404', '403', '302', '401', '200', '201', '500', '503', '304'].includes(curr.response_code)) {
        var key = `${curr.response_code}`;
        if (!acc[key]) {
          acc[key] = 1;
        } else {
          acc[key]++;
        }
      }
     
       return acc;
     }, {});
   }

  function extractRequestsPerMinute() {
    return epaDataRecords.reduce(function (acc, curr) {
      var key = `${curr.datetime.day}:${curr.datetime.hour}:${curr.datetime.minute}`;
       if (!acc[key]) {
         acc[key] = 1;
       } else {
         acc[key]++;
       }
     
       return acc;
     }, {});
   }
  
});
