import fs from 'fs';
import es from 'event-stream';
const chalk = require('chalk');

import * as UTILS from './utils';

let lnCount = 0;
const stream = fs.createReadStream('./epa-http.txt')
    .pipe(es.split())
    .pipe(es.mapSync(function(line) {

        stream.pause();

        lnCount++;
        const request_array = UTILS.cleanLogLine(line);
        const request_method = request_array[2]; // That's where request method is

        //log request
        UTILS.logRequestCounts(request_method);

        // formulate request only if request is valid and clean it to parse
        const jsonContent = UTILS.parseRequest(request_array);

        UTILS.content.push(jsonContent);

        stream.resume();

    })
    .on('error', function(err){
        console.log(chalk.red('Error while reading file.'));
        console.log(err);
    })
    .on('end', function() {
        console.log(chalk.yellow('\n\nRead entire file.\n'));
        console.log(chalk.blue('\n-----Request Count------\n'));
        console.log(UTILS.count_dict);
        console.log(chalk.blue('\n------------------------\n'));
        const content = "window.epadata='" + JSON.stringify(UTILS.content) + "'";
        UTILS.writeToFile(content);
        UTILS.resetContent();
    })
    
);


