import fs from 'fs';
const chalk = require('chalk');

export const VALID_REQUESTS = ['GET', 'POST', 'HEAD'];
export const count_dict = {'GET': 0, 'POST': 0, 'HEAD': 0, 'INVALID-REQUEST': 0};

export let content  = [];

export const parseRequest = request_array => {

    const [ host, 
        datetime_val, 
        method_str, 
        url, 
        protocol , 
        response_code, 
        document_size] = request_array;
        const method = (method_str !=='' && !VALID_REQUESTS.includes(method_str))? 'INVALID-REQUEST': method_str;
        
    //extract date info
    const [day, hour, minute, second] = getDate(datetime_val);
    //extract protocol info
    const [protocol_name, protocol_version] = getProtocol(protocol);;
    //extract request info
    const request = { method, url, protocol_name, protocol_version}
    //formulate final row/record to insert
    return {
        host,
        datetime: { day, hour, minute, second },
        request,
        response_code,
        document_size
    };
}
export const writeToFile = jsonContent => {
    fs.writeFile("./access_log_EPA_Jul95_parsed.json", jsonContent, 'utf8', function (err) {
        if (err) {
            console.log(chalk.red("An error occured while writing JSON Object to File."));
            return console.log(chalk.red(err));
        }
        console.log(chalk.green("JSON file has been saved.\n"));
    });
}

export const cleanLogLine = line => {
    line = line.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
    return line.replace(/[\"|\[\]]/g, '').split(" ");
}

export const getProtocol = protocol => {
    return protocol ? protocol.split('/'): '';
}

export const getDate = datetime_val => {
    return datetime_val ? datetime_val.split(':'): [];
}

export const logRequestCounts = (method) => {
    const key = VALID_REQUESTS.includes(method) ? method : 'INVALID-REQUEST';
    count_dict[key]++;
}

export const isRequestValid = method => {
    return VALID_REQUESTS.includes(method);
}

export const resetContent = () => {
    content = [];
}